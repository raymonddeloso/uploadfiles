<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Profile extends Model
{
    public function user() {
    	return $this->belongsTo('\App\User');
    }

    public static function getName($userid) {
    	$name = \App\Profile::where('user_id',$userid)->first();
    	return $name->firstname.' '.$name->lastname;
    }
}
