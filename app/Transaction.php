<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Transaction;
use App\File;

class Transaction extends Model
{
    protected $table = 'transaction_logs';

    public static function login($userid)
    {	
    	$trans = new Transaction;
    	$trans->user_id = $userid;
    	$trans->transaction = "Logged In";
    	$trans->save();
    }

    public static function logout($userid)
    {
    	$trans = new Transaction;
    	$trans->user_id = $userid;
    	$trans->transaction = "Logged Out";
    	$trans->save();
    }

    public static function uploadFile($userid,$fileid)
    {
    	$file = File::find($fileid);

    	$trans = new Transaction;
    	$trans->user_id = $userid;
    	$trans->transaction = "Uploaded the file ".$file->name;
    	$trans->save();
    }

    public static function previewFile($userid,$fileid)
    {
    	$file = File::find($fileid);

    	$trans = new Transaction;
    	$trans->user_id = $userid;
    	$trans->transaction = "Preview the file ".$file->name;
    	$trans->save();
    }

    public static function downloadFile($userid,$fileid)
    {
    	$file = File::find($fileid);

    	$trans = new Transaction;
    	$trans->user_id = $userid;
    	$trans->transaction = "Downloaded the file ".$file->name;
    	$trans->save();
    }

    public static function  moveArchive($userid,$fileid)
    {
    	$file = File::find($fileid);

    	$trans = new Transaction;
    	$trans->user_id = $userid;
    	$trans->transaction = "Moved the file ".$file->name." to archived";
    	$trans->save();
    }

    public static function deletePermanent($userid,$fileid)
    {
    	$file = File::onlyTrashed()->find($fileid);

    	$trans = new Transaction;
    	$trans->user_id = $userid;
    	$trans->transaction = "Deleted the ".$file->name." file permanently";
    	$trans->save();
    }
}
