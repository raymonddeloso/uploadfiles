<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/',function(){
	if (Auth::check()) {
		return redirect()->route('index');
	}
	return view('login');
});

Route::get('/register', function(){
	if (Auth::check()) {
		return redirect()->route('index');
	}
	return view('register');
});	

Route::post('/register',['as'=>'register','uses'=>'HomeController@register']);

Route::post('/login','HomeController@login');
Route::get('/logout','HomeController@logout');

Route::group(['middleware'=>'logged'],function(){

	Route::get('index',['as'=>'index',function(){return view('index');}]);
	Route::get('/all/files',['as'=>'allfiles',function(){return view('allfiles');}]);
	Route::get('/my/files',['as'=>'myfiles',function(){return view('users.myfiles');}]);
	Route::get('/my/archived-files',['as'=>'archived',function(){return view('users.myarchivedfiles');}]);

	Route::post('upload-files',['as'=>'uploadfiles','uses'=>'FileController@uploadFiles']);
	Route::post('delete-file','FileController@deleteFile');
	Route::get('download-file/{id}',['as'=>'downloadfile','uses'=>'FileController@downloadFile']);
	Route::get('preview-file/{id}',['as'=>'previewfile','uses'=>'FileController@previewFile']);
	Route::post('delete-file-permanent','FileController@deletePermanent');

});


