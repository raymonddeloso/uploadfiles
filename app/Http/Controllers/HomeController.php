<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use App\User;

use App\Profile;

use Auth;

class HomeController extends Controller
{

	public function register(Request $request) {


		$user = new User;
		$user->username = $request->username;
		$user->password = bcrypt($request->password);
		$user->save();

		$profile = new Profile;
		$profile->firstname = $request->firstname;
		$profile->middlename = $request->middlename;
		$profile->lastname = $request->lastname;
		$profile->position = $request->position;
		$profile->employment_type = $request->employment_type;
		$profile->user_id = $user->id;
		$profile->save();
		

	}

    public function login(Request $request)
    {
    	$username = User::where('username',$request->username)->first();

    	if (empty($username)) {
    		return redirect()->back()->with('unregistered','Unregistered Username!');
    	}

    	if (Auth::attempt(['username'=>$request->username,'password'=>$request->password])) {
            \App\Transaction::login(Auth::User()->id);
    		return redirect()->route('index');
    	} else {
    		return redirect()->back()->with('incorrect','Incorrect Password!');
    	}
    }

    public function logout() {
        \App\Transaction::logout(Auth::User()->id);
    	Auth::logout();
    	return redirect('/');
    }

}
