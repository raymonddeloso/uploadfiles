<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use App\File;

use Auth,Storage,Response;

use App\Transaction;

class FileController extends Controller
{

    public function uploadFiles(Request $request)
    {
        $files = $request->file('file');


        if (!is_null($files[0])) {

            foreach ($files as $file) {

                $randomString = time().'-';

                $destinationPath = storage_path().'/files/'.$file->getClientOriginalExtension();

                $fileLink = '/files/'.$file->getClientOriginalExtension().'/'.$randomString.$file->getClientOriginalName();

                $savefile = new File;
                $savefile->user_id = Auth::User()->id;
                $savefile->name = $request->filename;
                $savefile->type = $file->getClientOriginalExtension();
                $savefile->path = $fileLink;
                $savefile->mime_type = $file->getMimeType();
                $savefile->save();

                $file->move($destinationPath,$randomString.$file->getClientOriginalName());

                Transaction::uploadFile(Auth::User()->id,$savefile->id);
            }
        }

    	return redirect()->back()->with('saved',true);
    }

    public function deleteFile(Request $request)
    {
        $file = File::find($request->id);
        Transaction::moveArchive(Auth::User()->id, $file->id);
        File::Destroy($request->id);
        return 'ok';
    }

    public function downloadFile($id)
    {
        $file = File::find($id);
        Transaction::downloadFile(Auth::User()->id,$file->id);
        return response()->download(storage_path().$file->path);

    }

    public function previewFile($id)
    {
        //office editing for docs, sheets and slides
        $file = File::find($id);
        Transaction::previewFile(Auth::User()->id,$file->id);
        //return response()->file(storage_path().$file->path,['Content-Type'=>$file->mime_type]);
        $previewfile = file_get_contents(storage_path().$file->path);

        $response = Response::make($previewfile, 200);

        $response->header('Content-Type',$file->mime_type);

        return $response;
    }

    public function deletePermanent(Request $request){
        $file = File::withTrashed()->find($request->id);

        if (!empty($file->path)) {
            Transaction::deletePermanent(Auth::User()->id,$request->id);
            $path = implode("\\",explode('/', $file->path));
          
            \Storage::delete($path);
            
            $file->forceDelete();

            return 'ok';
        }
        return 'failed';
    }
}
