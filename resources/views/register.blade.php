@extends('layout.masterlayout')

@section('content')
	<div class="container">
		<div class="row" style="padding-top:50px">
			<div class="col-md-3"></div>
			<div class="col-md-6">
				<div class="panel panel-primary">
					<div class="panel-heading">
						<h3 class="panel-title"><b>Create</b> your account here</h3> 
					</div>
					<div class="panel-body">
						<form action="{{route('register')}}" method="post">
							<div class="row">
								<div class="col-md-4">
									<div class="form-group">
										<label for="">Username</label>
										<input type="text" class="form-control" name="username" id="username" placeholder="Username" required>
									</div>
								</div>
								<div class="col-md-4">
									<div class="form-group">
										<label for="">Password</label>
										<input type="password" name="password" id="password" placeholder="Password" class="form-control" required>
									</div>
								</div>
								<div class="col-md-4">
									<div class="form-group">
										<label for="">Re-Type Password</label>
										<input type="password" name="repassword" id="repassword" placeholder="Re-Type Password" class="form-control" required>
									</div>
								</div>
							</div>
							<hr>
							<div class="row">
								<div class="col-md-4">
									<div class="form-group">
										<label for="">Firstname</label>
										<input type="text" name="firstname" id="firstname" placeholder="Firstname" class="form-control" required>
									</div>
								</div>
								<div class="col-md-4">
									<div class="form-group">
										<label for="">Middlename</label>
										<input type="text" name="middlename" id="middlename" placeholder="Middlename" class="form-control" required>
									</div>
								</div>
								<div class="col-md-4">
									<div class="form-group">
										<label for="">Lastanem</label>
										<input type="text" name="lastname" id="lastname" placeholder="Lastname" class="form-control" required>
									</div>
								</div>
							</div>
							<div class="row">
								<div class="col-md-4">
									<div class="form-group">
										<label for="">Position</label>
										<select name="position" id="position" class="form-control">
											<option value="">Select Position</option>
											<option value="Clerk">Clerk</option>
										</select>
									</div>
								</div>
								<div class="col-md-4">
									<label for="">Employment Type</label>
									<select class="form-control" name="employmenttype" id="employementtype">
										<option value="">Employment Type</option>
										<option value="Permanent">Permanent</option>
										<option value="Job Order">Job Order</option>
										<option value="Contructual">Contructual</option>
									</select>
								</div>
								<div class="col-md-4"></div>
							</div>
							<br>
							<div class="row">
								<div class="form-group">
									<button type="submit" class="btn btn-success center-block">Register</button>
								</div>
							</div>
						</form>
					</div>
					<div class="panel-footer">
						already have an account? <a href="/">Login</a>
					</div>
				</div>
			</div>
			<div class="col-md-3"></div>
		</div>
	</div>
@stop