<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>FIS | File Information System</title>
	<link rel="stylesheet" href="{{asset('src/css/bootstrap.min.css')}}">
  <link rel="stylesheet" href="{{asset('src/media/css/jquery.dataTables.min.css')}}">
  <link rel="stylesheet" href="{{asset('src/sweetalert/sweetalert.css')}}">
  <meta name="csrf-token" content="{{ csrf_token() }}">
  @stack('css')
</head>
<body>
<nav class="navbar navbar-default">
  <div class="container">
    <!-- Brand and toggle get grouped for better mobile display -->
    <div class="navbar-header">
      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
      <a class="navbar-brand" href="/">File Management System</a>
    </div>

    <!-- Collect the nav links, forms, and other content for toggling -->
    @if(Auth::check())
    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
      
      <ul class="nav navbar-nav">
        <li 
        @if(\Request::route()->getName() == "myfiles")
        class="active"
        @endif><a href="{{route('myfiles')}}">My Files <span class="sr-only">(current)</span></a></li>
        <li 
        @if(\Request::route()->getName() == "archived")
        class="active"
        @endif><a href="{{route('archived')}}">My Achived Files</a></li>
        <li
        @if(\Request::route()->getName() == "allfiles")
        class="active"
        @endif><a href="{{route('allfiles')}}">All Files</a></li>
        
      </ul>
 
      <ul class="nav navbar-nav navbar-right">
        <li class="dropdown">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">{{Auth::User()->profile->firstname.' '.Auth::User()->profile->lastname}} <span class="caret"></span></a>
          <ul class="dropdown-menu">
            <li><a href="/logout">logout</a></li>
            
        </li>
      </ul>
    </div><!-- /.navbar-collapse -->
    @endif
  </div><!-- /.container-fluid -->
</nav>
@yield('content')
<script type="text/javascript" src="{{asset('src/jquery/jquery.min.js')}}"></script>
<script type="text/javascript" src="{{asset('src/js/bootstrap.min.js')}}"></script>
<script type="text/javascript" src="{{asset('src/media/js/jquery.dataTables.min.js')}}"></script>
<script type="text/javascript" src="{{asset('src/sweetalert/sweetalert.min.js')}}"></script>
@stacK('js')
</body>
</html>