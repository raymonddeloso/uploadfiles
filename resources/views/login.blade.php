@extends('layout.masterlayout')
@section('content')
<div class="container">
	<div class="row" style="padding-top:100px;">
		<div class="col-md-4"></div>
		<div class="col-md-4">
			<div class="panel panel-primary">
			  <div class="panel-heading">
			    <h3 class="panel-title"><b>Login</b> to start your session</h3>
			  </div>
			  <div class="panel-body">
		   		<form action="/login" method="post">
		   			{{csrf_field()}}
		   			@if(session('unregistered'))
		   				<div class="alert alert-danger alert-dismissible" role="alert">
						  <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
						  <strong>Oops!</strong> {{session('incorrect')}}
						</div>
		   			@elseif(session('incorrect'))
		   				<div class="alert alert-danger alert-dismissible" role="alert">
						  <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
						  <strong>Oops!</strong> {{session('incorrect')}}
						</div>
		   			@endif
		   			<div class="form-group">
			   			<label for="">Username</label>
			   			<div class="input-group">
						  <span class="input-group-addon" id="basic-addon1"><span class="glyphicon glyphicon-user"></span></span>
						  <input type="text" class="form-control" name="username" id="username" placeholder="Username" aria-describedby="basic-addon1" required>
						</div>
			   		</div>
			   		<div class="form-group">
			   			<label for="">Password</label>
			   			<div class="input-group">
						  <span class="input-group-addon" id="basic-addon1"><span class="glyphicon glyphicon-asterisk"></span></span>
						  <input type="password" class="form-control" id="password" name="password" placeholder="Password" aria-describedby="basic-addon1" required>
						</div>
			   		</div>
			   		<div class="form-group">
			   			<button class="btn btn-success center-block"><span class="glyphicon glyphicon-log-in"></span> Login</button>
			   		</div>
			   		<div class="form-group">
			   			<a href="/register">Register</a>
			   		</div>
		   		</form>
			  </div>
			</div>
		</div>
		<div class="col-md-4"></div>
	</div>
</div>
@stop