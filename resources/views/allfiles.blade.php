@extends('layout.masterlayout')
@section('content')
<div class="container">
	<div class="row">
		<div class="col-md-12">
			<table id="allfiles" class="table table-striped">
				<thead>
					<th >File Name</th>
					<th >Uploaded By</th>
					<th class="text-center">Uploaded Date</th>
					<th class="text-center">Type</th>
					<th class="text-center">Option</th>
				</thead>
				<tbody>
					@foreach(\App\File::where('user_id',Auth::User()->id)->orderBy('id','DESC')->get() as $file)
					<tr>
						<td>{{$file->name}}</td>
						<td>{{\App\Profile::getName($file->user_id)}}</td>
						<td width="15%" class="text-center">
							<?php
							$date = new Carbon\Carbon($file->created_at);
							echo $date->toFormattedDateString();

							?>
						</td>
						<td width="12%" class="text-center">{{$file->type}}</td>
						<td width="10%">
								<a href="{{route('downloadfile',$file->id)}}" class="btn btn-primary btn-sm " id="open" data-id="{{$file->id}}" title="download"><span class="glyphicon glyphicon-arrow-down"></span> </a> |
								<a href="{{route('previewfile',$file->id)}}" target="_blank"class="btn btn-success btn-sm" id="preview" data-id="{{$file->id}}" title="preview"><span class="glyphicon glyphicon-search"></span></a>
								
						</td>
					</tr>
					@endforeach
				</tbody>
			</table>
		
		</div>
	</div>
</div>

@stop
@push('js')

<script type="text/javascript">
	@if(session('saved'))
		swal('Success','Filed Saved!','success');
	@endif
	$(function(){

		var _url = location.protocol+'//'+location.host;

		$('#allfiles').DataTable({
			"bSort":false
		});

	

	});

</script>
@endpush