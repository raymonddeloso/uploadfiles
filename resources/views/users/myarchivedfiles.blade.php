@extends('layout.masterlayout')
@section('content')
<div class="container">
	<div class="row">
		<div class="col-md-12">
			<table id="archivedfiles" class="table table-striped">
				<thead>
					<th >File Name</th>
					<th class="text-center">Archived Date</th>
					<th class="text-center">Type</th>
					<th class="text-center">Option</th>
				</thead>
				<tbody>
					@foreach(\App\File::onlyTrashed()->where('user_id',Auth::User()->id)->orderBy('id','DESC')->get() as $file)
					<tr>
						<td>{{$file->name}}</td>
						<td width="15%" class="text-center">
							<?php
							$date = new Carbon\Carbon($file->deleted_at);
							echo $date->toFormattedDateString();

							?>
						</td>
						<td width="12%" class="text-center">{{$file->type}}</td>
						<td width="10%">
							
								<button class="btn btn-danger btn-sm center-block" id="delete" data-id="{{$file->id}}" title="Delete Permanently"><span class="glyphicon glyphicon-trash"></span> </button>
								
							
						</td>
					</tr>
					@endforeach
				</tbody>
			</table>

		</div>
	</div>
</div>


@stop
@push('js')

<script type="text/javascript">
	@if(session('saved'))
		swal('Success','Filed Saved!','success');
	@endif
	$(function(){

		var _url = location.protocol+'//'+location.host;

		$('#archivedfiles').DataTable({
			"bSort":false
		});

		$('button[id="delete"]').click(function(){
			var docid = $(this).attr('data-id');

			swal({   
				title: "Delete Permanently?",   
				text: "Are you sure you want to delete this file permanently?!",   
				type: "warning",   
				showCancelButton: true,   
				confirmButtonColor: "#DD6B55",   
				confirmButtonText: "Yes, Trash it!",   
				closeOnConfirm: false }, 
				function(){   
					$.ajax({
						type:"post",
						headers:{
							'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
						},
						url:_url+'/delete-file-permanent',
						data:{id:docid},
						success:function(data){
							if (data == 'ok') {
								swal('Deleted!','File deleted permanently!','success');
								setTimeout(function(){
					    			location.reload();
					    		},1500);
							} else {
								swal('Error!','Failed to delete file permanently!','error');
							}
						}

					});
				});
			});
	
		});

		

</script>
@endpush