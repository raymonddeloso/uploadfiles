@extends('layout.masterlayout')
@section('content')
<div class="container">
	<div class="row">
		<div class="col-md-12">
			<table id="myfiles" class="table table-striped">
				<thead>
					<th >File Name</th>
					<th class="text-center">Uploaded Date</th>
					<th class="text-center">Type</th>
					<th class="text-center">Option</th>
				</thead>
				<tbody>
					@foreach(\App\File::where('user_id',Auth::User()->id)->orderBy('id','DESC')->get() as $file)
					<tr>
						<td>{{$file->name}}</td>
						<td width="15%" class="text-center">
							<?php
							$date = new Carbon\Carbon($file->created_at);
							echo $date->toFormattedDateString();

							?>
						</td>
						<td width="12%" class="text-center">{{$file->type}}</td>
						<td width="18%">
							
								<button class="btn btn-danger btn-sm " id="archive" data-id="{{$file->id}}" title="Move to Archive"><span class="glyphicon glyphicon-trash"></span> </button> | 
								<a href="{{route('downloadfile',$file->id)}}" class="btn btn-primary btn-sm " id="open" data-id="{{$file->id}}" title="download"><span class="glyphicon glyphicon-arrow-down"></span> </a> |
								<a href="{{route('previewfile',$file->id)}}" target="_blank"class="btn btn-success btn-sm" id="preview" data-id="{{$file->id}}" title="preview"><span class="glyphicon glyphicon-search"></span></a>
							
						</td>
					</tr>
					@endforeach
				</tbody>
			</table>
		<button type="button" class="btn btn-success" data-toggle="modal" data-target="#uploadnewfile"><span class="glyphicon glyphicon-plus-sign"></span> Upload New File</button>
		</div>
	</div>
</div>

<div class="modal fade" tabindex="-1" role="dialog" id="uploadnewfile">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
    	<form action="{{route('uploadfiles')}}" method="post" enctype="multipart/form-data">
    			{{csrf_field()}}
		      <div class="modal-header">
		        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
		        <h4 class="modal-title">Upload New File</h4>
		      </div>
		      <div class="modal-body">
		       	<div class="form-group">
		       		<label for="">File Name</label>
		       		<input type="text" name="filename" id="filename" class="form-control" Placeholder="file name" required>
		       	</div>
		       	<div class="form-group">
		       		<label for="">Choose File</label>
		       		<input type="file" name="file[]" id="file" multiple required>
		       		<span class="help-block">You can upload multiple files
		       		 by holding the Ctrl button and choose a file</span>
		       	</div>
		      </div>
		      <div class="modal-footer">
		      	<button type="submit" class="btn btn-primary">Save changes</button>
		        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
		        
		      </div>
      	</form>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

@stop
@push('js')

<script type="text/javascript">
	@if(session('saved'))
		swal('Success','Filed Saved!','success');
	@endif
	$(function(){

		var _url = location.protocol+'//'+location.host;

		$('#myfiles').DataTable({
			"bSort":false
		});

		$('button[id="archive"]').click(function(e){

			var documentid = $(this).attr('data-id');
			
			swal({   
				title: "Move to archive?",   
				text: "Are you sure you want to move this to archive!",   
				type: "warning",   
				showCancelButton: true,   
				confirmButtonColor: "#DD6B55",   
				confirmButtonText: "Yes, Archive it!",   
				closeOnConfirm: false }, 
				function(){   
					$.ajax({
						type:"post",
						url:_url+'/delete-file',
						headers: {
					        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
					    },
					    data:{id:documentid},
					    success:function(data){
					    	if (data == 'ok') {
					    		swal('Archived!','Successfully moved to archived!','success');
					    		setTimeout(function(){
					    			location.reload();
					    		},1500);
					    	}
					    }
					})
				});
		});

		$('button[id="open"]').click(function(e){
			var documentid = $(this).attr('data-id');
			$.ajax({
				type:"get",
				url:_url+'/download-file/'+documentid,
				success:function(data) {
					console.log(data);
				}
			});

		});

	});

</script>
@endpush