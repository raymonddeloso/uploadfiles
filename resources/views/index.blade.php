@extends('layout.masterlayout')
@section('content')
<div class="container">
	<div class="col-md-9">
		<div class="panel panel-info">
			<div class="panel-heading">
				Search File
			</div>
			<div class="panel-body">
				<div class="row">
					<div class="col-md-4">
						<div class="form-group">
							<label for="">Name</label>
							<input type="text" name="filename" id="filename" Placeholder="File Name" class="form-control">
						</div>
					</div>
					<div class="col-md-3">
						<div class="form-group">
							<label for="">Type</label>
							<select name="type" id="type" class="form-control">
								<option value="">Select Type</option>
								@foreach(\App\File::groupBy('type')->get(['type']) as $type)
									<option value="">{{$type->type}}</option>
								@endforeach
							</select>
						</div>
					</div>
					<div class="col-md-3">
						<div class="form-group">
							<label for="">Date Saved</label>
							<input type="date" class="form-control">
						</div>
					</div>
					<div class="col-md-2">
						<div class="form-group">
							<label for="">&nbsp;</label>
							<button class="btn btn-success btn-block"><span class="glyphicon glyphicon-search"></span> Search</button>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-md-12">
						<table class="table table-striped">
							<thead class="success">
								<th>Name</th>
								<th>Uploaded By</th>
								<th>Date</th>
								<th>Info</th>
							</thead>
							<tbody>
								
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="col-md-3">
		<div class="panel panel-success">
		  <div class="panel-heading"><b>Welcome!</b> {{Auth::User()->profile->firstname}}</div>
		  <div class="panel-body">
		    Panel content
		  </div>
		</div>
	</div>

</div>
@stop